;; IO macro subroutines, definitions and constants
;;

;; 
;; VIA1
;;

VIA1_BASE   equ $6000
VIA1_PORTB  equ VIA1_BASE
VIA1_PORTA  equ (VIA1_BASE+$1)
VIA1_DDRB   equ (VIA1_BASE+$2)
VIA1_DDRA   equ (VIA1_BASE+$3)
VIA1_PCR    equ (VIA1_BASE+$c)
VIA1_IFR    equ (VIA1_BASE+$d)
VIA1_IER    equ (VIA1_BASE+$e)

;; ACIA

;; ACIA1
ACIA1_BASE    equ $4400
ACIA1_DATA    equ (ACIA1_BASE)
ACIA1_STATUS  equ (ACIA1_BASE+$1)
ACIA1_COMMAND equ (ACIA1_BASE+$2)
ACIA1_CONTROL equ (ACIA1_BASE+$3)

init_acia1:	
  ;  lda #%00000000          ; 8-bit, 1 stop, 115200 baud
  lda #%00011111        ;1 stop bit, 8 data bits, 19200 baud
  sta ACIA1_CONTROL
  lda #%00001011        ;No parity, no echo, no interrupt
  sta ACIA1_COMMAND
  rts

;; Expects the character to write in register A
putch:	
putch_acia1:	
  tax
putch_acia1_wait_txt_empty:	
  lda ACIA1_STATUS
  and #$10
  beq putch_acia1_wait_txt_empty
  txa
  sta ACIA1_DATA
  rts

;; Write a string to ACIA1
;; Expects the zero-terminated string to be in the `string` parameter
;; This is wrapped by a macro to put the message address into the work
;; area first, which means this function is not reentrant.

putstr macro string
   store_addr \string, str_work
   jsr _putstr
   endm

_putstr:	
   phy
   phx
   ldy #0
_putstr_1:	
   lda (str_work), y
   beq _putstr_done
   jsr putch
   iny
   jmp _putstr_1
_putstr_done:	
   plx
   ply
   rts

;; Waits for a character and return it in A
getch:	
getch_acia1:	
getch_acia1_wait_rxd_full:	
  lda ACIA1_STATUS
  and #$08
  beq getch_acia1_wait_rxd_full
  lda ACIA1_DATA
  rts

;; Read a line of characters in.  Stops when it receives a $0d
;; character (CR)
getstr macro buffer
  store_addr \buffer, str_work
  jsr _getstr
  endm

_getstr:	
  phy
  pha
  ldy #0
_getstr_1:	
  jsr getch
  cmp #$0a 											; Just ignore the LF for now
  beq _getstr_1
  cmp #$0d
  beq _getstr_2
  sta (str_work), y
  iny
  jmp _getstr_1
_getstr_2:	
  lda #$00
  ;;iny
  sta (str_work), y
  pla
  ply
  rts

delay_6551:     phy
                phx
delay_loop:     ldy #1
minidly:        ldx #$68
delay_1:        dex
                bne delay_1
                dey
                bne minidly
                plx
                ply
delay_done:     rts

