
# Assembler flags

ASM=vasm6502_oldstyle
#ASMFLAGS=-Fbin -dotdir
ASMFLAGS=-Fbin -I../lib -c02

# Programmer

PROGRAMMER=minipro
EEPROM=AT28C256
PROGFLAGS=-p ${EEPROM} -w

%.bin : %.asm
	${ASM} -L $<.lst ${ASMFLAGS} -o $@ $<

program: all
	@if [ "$(IMG)" = "" ]; then \
		echo "binary missing (make program IMG=binary)"; \
	else \
		echo $(PROGRAMMER) $(PROGFLAGS) $(IMG); \
		$(PROGRAMMER) $(PROGFLAGS) $(IMG); \
	fi

clean:
	rm -f *.bin *.lst *.hex
