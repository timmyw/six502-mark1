;; Data table in RAM

;; Zero page
;; Data

temp1       equ $00        ; 1 byte
temp2       equ $02				 ; 2 bytes

str_work    equ $10             ; 2 bytes - Used to store address of
																; message to print
strcpy_src  equ $12             ; 2 bytes
strcpy_dest equ $14             ; 2 bytes
strcpy_len  equ $16             ; 1 byte

conv_dest   equ $17             ; 2 bytes

hex_buffer  equ $19             ; 5 bytes
hex_buffer2 equ $20             ; 3 bytes

current_location equ $23        ; 2 bytes: current list/dump address
current_count    equ $25				; 2 bytes: count for list/dump

command_buffer equ $0200				; 255 bytes
output_build   equ $0300				; 10 bytes
output_buffer  equ $030a				; 245 bytes
output_count   equ $0400        ; 1 byte
command_params equ $0401				; 20 bytes - 10 parameter pointers

address_build  equ $0415				; 2 bytes
address_source equ $0417				; 2 bytes

next_avail     equ $0419				; Next byte

output_buffer_len equ 245
