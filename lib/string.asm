;; String utilities
;;

;; Copy the null terminated string from src to dest.  
;; Doesn't pay attention to buffer sizes
strcpy macro src, dest, len
  store_addr \src, strcpy_src
  store_addr \dest, strcpy_dest
  pha
  phy
  lda #\len
  sta strcpy_len
  jsr _strcpy
  ply
  pla
  endm

_strcpy:	
  ldy #0
_strcpy_1:
  lda strcpy_len                ; First check we have space left
  beq _strcpy_2
  dec
  sta strcpy_len
  lda (strcpy_src), y           ; Copy a char and loop
  sta (strcpy_dest), y
  iny
  cmp #0
  bne _strcpy_1
  rts
_strcpy_2:                      ; We got here because we ran out of space
  sta (strcpy_dest), y          ; null-terminate what we have and return (we
	                              ; already have 0 in A)
  rts

;; Concatenate strings.
;; Note that \len here is not the length to copy but the length of the dest
;; buffer
strcat macro src, dest, len
  store_addr \dest, strcpy_dest
  store_addr \src, strcpy_src
  lda #\len
  sta strcpy_len
  jsr _strcat
  endm

_strcat:
  ;; First find end of dest
  ldy #0
_strcat_1:
  lda (strcpy_dest), y
  beq _strcat_2
  iny
  jmp _strcat_1
_strcat_2:
  ;; y is now the correct offset in
  tya
  sta temp1
  lda strcpy_dest
  clc
  adc temp1
  sta strcpy_dest
  lda strcpy_dest+1
  adc #0
  sta strcpy_dest+1
  jsr _strcpy
  rts

;; "Cleanses" the character in A register.  Anything unprintable (<32
;; or >127) is switched to '.'
clean_char:	
  cmp #' '											; Check for < 32
  bcc clean_char_1
  cmp #$80											; Check for < 127
  bcs clean_char_1
  rts
clean_char_1:	
  lda #'.'
  rts

