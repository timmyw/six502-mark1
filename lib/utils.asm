;; Utility macros and definitions

;; Initialise the stack
init_stack macro
  ldx #$FF
  txs
  endm

;; Stores the \value word in to the memory pointed to by \dest
store_addr macro value, dest
  pha
  lda #(\value & $ff)
  sta \dest
  lda #(\value >> 8)
  sta \dest+1
  pla
  endm

store_value_ind macro source, dest
  pha
  lda \source
  sta \dest
  lda \source + 1
  sta \dest + 1
  pla
  endm

storestate macro 
  pha
  phx
  phy
  endm

restorestate macro
  ply
  plx
  pla
  endm

;; Expect command_buffer,x to point to beginning of hex string
cmd_parse_hex_8:	
  lda command_buffer, x
  jsr parse_hex_digit
  asl
  asl
  asl
  asl
  sta temp2
  inx
  lda command_buffer, x
  jsr parse_hex_digit
  ;; Need to add lower 4 bits from a to the first higher nibble
  clc
  adc temp2 
  rts

parse_hex_digit:	
  ;; Check between 'a' and 'f'
  ;; Check GT 'f'
  cmp #'f' + 1
  bcs phd_done 										; phd_done
  ;; Check GTE 'a'
  cmp #'a'
  bcc phd_0											; phd_0
  sec
  sbc #('a'-10)
  rts
phd_0:	
  ;; Check between 'A' and 'F'
  ;; Check greater than 'F'
  cmp #'F' + 1
  bcs phd_done
  ;; Check greater than 'A'
  cmp #'A'
  bcc phd_1
  sec
  sbc #('A'-10)
  rts
phd_1:	
  ;; Check between '0' and '9'
  ;; Check greater than '9'
  cmp #'9' + 1
  bcs phd_done
  ;; Check greater than '0'
  cmp #'0'
  bcc phd_2
  sec
  sbc #('0')
phd_2:	
phd_done:	
  rts
