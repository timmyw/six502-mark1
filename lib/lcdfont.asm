;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Contains the data for 0-F single LCD digit.
;;

lcdfont:

;; Bits controlling LED display segments.
SEG_A EQU (1<<0)
SEG_B EQU (1<<1)
SEG_C EQU (1<<2)
SEG_D EQU (1<<3)
SEG_E EQU (1<<4)
SEG_F EQU (1<<5)
SEG_G EQU (1<<6)

  db ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F) ; 0
	db ~(SEG_B|SEG_C)												 ; 1
	db ~(SEG_A|SEG_B|SEG_D|SEG_E|SEG_G)			 ; 2
	db ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_G)			 ; 3
	db ~(SEG_B|SEG_C|SEG_F|SEG_G)						 ; 4
	db ~(SEG_A|SEG_C|SEG_D|SEG_F|SEG_G)			 ; 5
	db ~(SEG_A|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G) ; 6
	db ~(SEG_A|SEG_B|SEG_C)									 ; 7
	db ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G) ; 8
	db ~(SEG_A|SEG_B|SEG_C|SEG_F|SEG_G)						 ; 9
	db ~(SEG_A|SEG_B|SEG_C|SEG_E|SEG_F|SEG_G)			 ; A
	db ~(SEG_C|SEG_D|SEG_E|SEG_F|SEG_G)						 ; b
	db ~(SEG_D|SEG_E|SEG_G)												 ; C
	db ~(SEG_B|SEG_C|SEG_D|SEG_E|SEG_G)						 ; d
	db ~(SEG_A|SEG_D|SEG_E|SEG_F|SEG_G)						 ; E
	db ~(SEG_A|SEG_E|SEG_F|SEG_G)
