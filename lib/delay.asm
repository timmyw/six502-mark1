;; Delay routines

;; Counts down x * y
;; Destroys x and y
delay_long:	
  phy
  phx
	ldy #$ff
delay_long_0:	
	ldx #$ff
delay_long_1:
	dex
	bne delay_long_1
	dey
	bne delay_long_0
  plx
  ply
  rts

;; Short delay
short_delay:	
  phx
  pha
  ldx #$ff
short_delay_0:
  dex
  bne short_delay_0
  pla
  plx
  rts
