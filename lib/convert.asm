;; Various conversion subroutines and macros
;;


    ;; Print a 16 bit number
    ;; Assumes two eight bit numbers in \num
print_hex_16 macro num, dest
  lda \num+1
  print_hex_8 hex_buffer
  lda \num
  print_hex_8 hex_buffer2
  strcpy hex_buffer, \dest, 4+1
  strcat hex_buffer2, \dest, 4+1
  endm

;; Print 8 bit hex number
print_hex_8 macro dest
  store_addr \dest, conv_dest
  jsr _print_hex_8
  endm

_print_hex_8:
  pha
  ;; Upper nibble
  lsr
  lsr
  lsr
  lsr

  tax
  ldy #0
  lda hexchars, x
  sta (conv_dest), y
  iny

;;;  Lower nibble
  pla
  and #$f
  tax
  lda hexchars, x
  sta (conv_dest), y
  iny

  lda #0
  sta (conv_dest), y
  rts
