   org $8000

   jmp start

putstr_work dw $0000

store_addr macro value, dest
   lda #(\value & $ff)
   sta \dest
   lda #(\value >> 8)
   sta \dest+1
   endm

putstr macro addr
   store_addr \addr, putstr_work
   jsr _putstr
   endm

address equ message

start:	

   putstr message
   
   brk

_putstr:	
   ldx #0
   lda putstr_work, x
   beq _putstr_done
   jsr putch
   inx
_putstr_done:	
   rts

putch:	
   rts

message:	
    byte "Hello", $00
