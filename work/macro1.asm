store_addr macro value, dest
   lda #(\value & $ff)
   sta \dest
   lda #(\value >> 8)
   sta \dest+1
   endm

putstr macro addr
   store_addr \addr, $1122
   jsr $0033
   endm

   org $8000

   lda #($3344 & $ff)
   sta $1122
   lda #($3344 >> 8 & $ff)
   sta $1122+1

   store_addr $3344, $1122
   
   putstr $3344
