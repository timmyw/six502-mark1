;; blink.asm

;; Super simple test - sends 0b01010101 and then 0b10101010 to ACIA1_DATA
;;
;; This isn't testing ACIA integration, but the control signals set up
;; for ACIA1 will be tested.
;; 
;; The simplest test is just checking:
;; - RW goes low
;; - data bus contains $55

YCNT equ $ff
XCNT equ $ff
	
  org $8000

  jmp main

  include io.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main loop
main:
	lda #$55
	sta ACIA1_DATA

	ldy #YCNT
	ldx #XCNT
delay_1_:
	dex
	bne delay_1_
	dey
	bne delay_1_

	lda #$aa
	sta ACIA1_DATA

	ldy #YCNT
	ldx #XCNT
delay_2_:
	dex
	bne delay_2_
	dey
	bne delay_2_
	
	jmp main

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Includes

  include data.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Reset vector address
	org $fffc
	word main
	word $0000
