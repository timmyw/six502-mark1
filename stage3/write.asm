;; write.asm

;; Super simple test - sends 0b01010101 and then 0b10101010 to ACIA1_DATA
;;
;; This isn't testing ACIA integration, but the control signals set up
;; for ACIA1 will be tested.
;; 
;; The simplest test is just checking:
;; - RW goes low
;; - data bus contains $55

YCNT equ $ff
XCNT equ $ff
ACIA1_DATA equ $4400
	
  org $8000

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main loop
main:
	lda #$55
	sta ACIA1_DATA
	
	jmp main

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Includes


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Reset vector address
	org $fffc
	word main
	word $0000
