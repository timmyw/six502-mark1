;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; count-via1.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Local defines
COUNTER equ $1000               ; Counter storage (in RAM)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  org $8000

main:

  ;; Set up stack
  ldx #$FF
  txs

  ;; Initialise VIA1
  jsr init_via

  ;; Starting count of zero
  lda #$00
  sta COUNTER

loop:

  ;; Load the counter, display it and wait for a bit
  lda COUNTER
  jsr display
  jsr delay_long
  jsr delay_long

  ;; Increment A (thanks 6502), mask out the top nibble, and store it
  clc 
  adc #1
  and #$f
  sta COUNTER

  ;; Jump back to the start again
  jmp loop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
display: 
  pha
  tax
  lda lcdfont,x                 ; Load 7-segment font for X
  jsr store_via
  pla
  rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Initialises the pins we need for output
init_via: 
  lda #%11111111                ; All pins output
  sta VIA1_DDRA         
  lda #%00000001                ; Just pin 0 is output for Port B
  sta VIA1_DDRB
  rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Pushes the value in A to the VIA's Port A.  A is destroyed in the
;; process.
store_via: 
  sta VIA1_PORTA
  lda #$0                       ; Pulse the 'clock' pin - drop it low,
                                ; wait, and then bring it high again
  sta VIA1_PORTB
  jsr short_delay
  lda #$1                       ; We could write $ff here as the other
                                ; pins of Port B are input, and as far
                                ; as the 65C22 is concerned,
                                ; writing to an input port does nothing.
  sta VIA1_PORTB
  rts


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Includes

  include io.asm
  include delay.asm
  include lcdfont.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Reset vector address
  org $fffc
  word main
  word $0000
