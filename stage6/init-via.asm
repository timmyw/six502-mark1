  ;; init-via.asm
  ;; Very simple loop to push initialisation and data to the VIA to
	;; help test the decoding and control signals

  org $8000

main:

  lda #$ff
  sta $6000
  sta $6001

  lda $1000 ;; test a ram signal

  lda $ff
  sta $6002 ;; port b
  lda #$aa
  sta $6003 ;; port b

  jmp main



;; Vectors

  org $fffc
  word main
  word $0000
