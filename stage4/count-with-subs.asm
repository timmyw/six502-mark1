  ;; count-with-ram.asm

;; Output port address
OPORT equ $4000

COUNTER equ $1000

;; Delay counts
YCNT equ $ff
XCNT equ $ff

  org $8000

main:

  ;; Set up stack
  ldx #$FF
  txs

  ;; Starting count of zero
  lda #$00
  sta COUNTER

loop:

  lda COUNTER
  jsr display
  jsr delay
  jsr delay

  clc 
  adc #1
  and #$f
  sta COUNTER
  lda #$0												; Forcing the test of ram
  jmp loop

display:	
  pha
  tax
  lda hexfont,x									; Load segment font for ~x~
  sta OPORT											; Write it out
  pla
  rts														;

  include delay.asm

hexfont:

;; Bits controlling LED display segments.
SEG_A EQU (1<<0)
SEG_B EQU (1<<1)
SEG_C EQU (1<<2)
SEG_D EQU (1<<3)
SEG_E EQU (1<<4)
SEG_F EQU (1<<5)
SEG_G EQU (1<<6)

  db ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F) ; 0
	db ~(SEG_B|SEG_C)												 ; 1
	db ~(SEG_A|SEG_B|SEG_D|SEG_E|SEG_G)			 ; 2
	db ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_G)			 ; 3
	db ~(SEG_B|SEG_C|SEG_F|SEG_G)						 ; 4
	db ~(SEG_A|SEG_C|SEG_D|SEG_F|SEG_G)			 ; 5
	db ~(SEG_A|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G) ; 6
	db ~(SEG_A|SEG_B|SEG_C)									 ; 7
	db ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G) ; 8
	db ~(SEG_A|SEG_B|SEG_C|SEG_F|SEG_G)						 ; 9
	db ~(SEG_A|SEG_B|SEG_C|SEG_E|SEG_F|SEG_G)			 ; A
	db ~(SEG_C|SEG_D|SEG_E|SEG_F|SEG_G)						 ; b
	db ~(SEG_D|SEG_E|SEG_G)												 ; C
	db ~(SEG_B|SEG_C|SEG_D|SEG_E|SEG_G)						 ; d
	db ~(SEG_A|SEG_D|SEG_E|SEG_F|SEG_G)						 ; E
	db ~(SEG_A|SEG_E|SEG_F|SEG_G)

  org $fffc
  word main
  word $0000
