build_buffer equ $0200
current_idx  equ $0300

;; Starting with most significant nibble
print_hex_num:	
  lda #$a9
  jsr to_hex_8
  strcpy build_buffer, current_idx, hex_build_buffer 

  putstr build_buffer

  rts

;; Convert 8 bits to hexadecimal.  Expects the 8 bits to be A
hex_build_buffer equ $0302 		; 3 bytes
to_hex_8:	
  tay														; Store 
  and #$f0											; most significant nibble
  lsr #4  
  tax
  lda hexchars, x
  sta hex_build_buffer

  tya
  and #$0f
  tax
  lda hexchars, x
  sta hex_build_buffer + 1

  lda #0
  sta hex_build_buffer + 2

  rts



;;
  cmp #$67
  bcs phd_done

  cmp #$61
  bcc phd_0
  sec
  sbc #87
  rts

phd_0:	
 ;; Check greater than 'F'
  cmp #$47
  bcs phd_1
  ;; Check greater than 'A'
  cmp #$41
  bcc phd_1
  sec
  sbc #55
  rts
phd_1:	
  ;; Check between '0' and '9'
  ;; Check greater than '9'
  cmp #$58
  bcs phd_2
  ;; Check greater than '0'
  cmp #$48
  bcc phd_2
  sec
  sbc #$48
phd_2:


LDA #51 ;; 'F'

  cmp #$67
  bcs phd_done

  cmp #$61
  bcc phd_0
  sec
  sbc #87
  rts
phd_0:	
 ;; Check greater than 'F'
  cmp #$47
  bcs phd_1
  ;; Check greater than 'A'
  cmp #$41
  bcc phd_1
  sec
  sbc #55
  rts
phd_1:	

phd_done:	

sta $0

;;
