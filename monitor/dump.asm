;; Implements the dump command

;; Check for up to 2 parameters:
;; Start address -> current_location
;; Length        -> current_count
execute_dump2:	
  rts

;; Dump a number of rows.  Loop over dump_16, and subtract 16 from
;; current count until current count drops below zero
dump_16s
  lda current_count
  sec
  sbc #$10
  sta current_count
  lda current_count+1
  sbc #$0
  sta current_count+1
  bmi dump_16s_done
  jsr dump_16										; Write a row
  jmp dump_16s
dump_16s_done:	
  rts

;; Dumps 16 bytes from current location
;; Updates current location and decrements current count
dump_16:	
  print_hex_16 current_location, output_build
  strcpy output_build, output_buffer, output_buffer_len
  strcat dump_delim1, output_buffer, output_buffer_len

  ldy #0
  ldx #$f												; 16 bytes as hex
dump_16_1:
  lda (current_location), y
 
  phx														; Save x and y as they will be
																; destroyed by the print routines
  phy
  print_hex_8 output_build
  strcat output_build, output_buffer, output_buffer_len
  strcat SPACE, output_buffer, output_buffer_len
  ply
  iny
  plx
  dex  
  beq dump_16_0
  jmp dump_16_1
dump_16_0:	

  putstr output_buffer
  putstr dump_delim1
  ldy #0
  ldx #0
  lda #$f
dump_16_2:											; 16 bytes as ascii
  pha	
  lda (current_location), y
  jsr clean_char
  sta output_buffer, x
  inx
  iny
  pla
  dec
  bne dump_16_2

  lda #0
  sta output_buffer, x
  putstr output_buffer

  putstr CRLF

  jsr update_location

  rts

update_location:	
  lda current_location
  clc
  adc #$10
  sta current_location
  bcc update_location_1
  lda current_location+1
  adc #0
  sta current_location+1
update_location_1:	
  rts
