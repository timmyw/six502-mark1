;; sixos monitor
;;

  org $8000

  ;; Jump over any code added by the includes
  jmp main

  include utils.asm
  include io.asm
  include string.asm
  include convert.asm

main:	

  init_stack

  jsr init_acia1
  jsr init_monitor

  putstr monitor_name

write:
  
  putstr monitor_prompt

  getstr command_buffer

  jsr process_command

  ;; Dump out output count 
  putstr CRLF
  putstr debug1
  lda output_count
  print_hex_8 output_build
  putstr output_build
  putstr SPACE
  putstr command_buffer
  putstr CRLF

  jsr execute_command

  jmp write

;; Execute the command.  Try and recognise the first parameter as a
;; command name
execute_command:	
  storestate

  ldy #$00
  lda command_params, y					; Index to start of parameters
  tax
  lda command_buffer, x					; Single char command in A

  cmp #'L'
  beq ec_run_dump
  cmp #'l'
  bne ec_0
ec_run_dump:	
  jsr execute_dump
  jmp ec_done

ec_0:	
 
ec_done:	
  restorestate
  rts


;; Expect x to index into command_buffer to beginning of 4 bytes for
;; the binary word
parse_hex_16:	
  phx
  jsr cmd_parse_hex_8						; A will now contain the 8 bit value
  sta address_build+1
  plx
  inx
  inx														; Jump to next "byte"
  jsr cmd_parse_hex_8
  sta address_build
  rts  
    

execute_dump:	
  ;; Parse parameter1
  lda output_count
  cmp #$01
  bne ed_cont1

  ;; Use current location, print one row
  jmp ed_ready_set_length

ed_cont1:	
 
  ldy #$01
  lda command_params, y
  tax														; At this point command_buffer, x
																; points to beginning of hex string

  jsr parse_hex_16

  ;; print_hex_16 address_build, output_build
  ;; putstr monitor_mark
  ;; putstr output_build
  ;; putstr CRLF

  store_value_ind address_build, current_location

  lda output_count
  cmp #$03											; Do we have a length
  beq ed_cont2
  jmp ed_ready_set_length
ed_cont2:												; Pull in length,
  ldy #$02
  lda command_params, y
  tax
  jsr parse_hex_16
  store_value_ind address_build, current_location
  jmp ed_ready

ed_ready_set_length:	
  store_addr $0010, current_count

ed_ready:	
  jsr dump_16s
  ;; Call the dump command
ed_done:	

  rts

;; Work through the command buffer, storing a pointer to each
;; parameter (word) into a list of pointers starting at
;; ~command_params~
;; Stop when we hit a zero
process_command:	
  storestate

  lda #$00
  sta output_count

  ldx #$00
  ldy #$00
pc_0:
  jsr pc_find_beginning
  stx temp1	
  beq pc_done
  
pc_1:	
  jsr pc_find_end
  inc output_count 							; We've found at least one

  ;; We stored the original offset in temp1
  lda temp1
  sta command_params, y
  iny

  bne pc_0

pc_done:	
  restorestate
  rts

;; Start from command_buffer,x moving forward until we hit a
;; null-terminator or a space.
pc_find_end:	
  lda command_buffer, x
  beq pc_fe_0
  cmp #' '
  beq pc_fe_0
  inx
  jmp pc_find_end
pc_fe_0:	
  rts

;; Start from command_buffer,x and move forward until we find either a
;; zero or a non-space character.  Leaves the first character in a,
;; and x pointing at that character
pc_find_beginning:	
  lda command_buffer, x
  beq pc_fb_0  									; null terminated
  cmp #' '
  bne pc_fb_0
  inx
  jmp pc_find_beginning
pc_fb_0:	
  rts

init_monitor:	
  lda #$00
  sta current_location
  lda #$80
  sta current_location+1

  lda #$00
  sta current_count
  lda #$01
  sta current_count+1

  rts

monitor_name:
  byte "Six502 Monitor v0.1 [002]", $0d, $0a, $00
monitor_prompt:	
  byte "READY>", $00

monitor_mark:	
  byte "#", $00
monitor_work:	
  byte "DUMP:", $00

debug1:	
  byte "CMD:", $00

CRLF:	
  byte $0d, $0a, $00
SPACE:	
  byte " ", $00
dump_delim1:	
  byte " : ", $00

hexchars:	
  byte "0123456789ABCDEF", $00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Includes

  include data.asm
  include delay.asm
  include lcdfont.asm

  include dump.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Reset vector address
  org $fffc
  word main
  word $0000


