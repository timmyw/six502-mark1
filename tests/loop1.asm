  ;; loop1.asm

  org $8000

main:
  ldx #$ff
  txs

  lda #$ff
  sta VIA1_DDRA
  sta VIA1_DDRB
  sta VIA1_PORTB

loop:	

  lda #$92
  sta VIA1_PORTA											; Write 5 to PortA

  ;; Pulse
  lda #$00
  sta VIA1_PORTB											; Write to IO
  lda #$ff
  sta VIA1_PORTB											; Write to IO

  jsr delay_long

  lda #$99
  sta VIA1_PORTA											; Write 4 to IO

  ;; Pulse
  lda #$00
  sta VIA1_PORTB											; Write to IO
  lda #$ff
  sta VIA1_PORTB											; Write to IO

  jsr delay_long

  jmp loop

_delay_long:	
	ldy #$ff
_delay_long_0:	
	ldx #$ff
_delay_long_1:
	dex
	bne delay_long_1
	dey
	bne delay_long_0
  rts

  include io.asm
;;  include lcdfont.asm
  include delay.asm

  org $fffc
  word main
  word $0000
