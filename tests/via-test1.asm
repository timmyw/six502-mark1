  ;; loop1.asm

OPORT equ $4000

  org $8000

  include io.asm

main:

  lda #$aa
  sta $1000											; Write to RAM

  lda #$55
  sta VIA1_PORTA											; Write to IO

  lda #$55
  sta VIA1_PORTB											; Write to IO

  jmp main

  org $fffc
  word main
  word $0000
