EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "six502"
Date ""
Rev "0.4"
Comp ""
Comment1 "(c) 2020-21"
Comment2 "tim@zipt.co"
Comment3 "Author: Tim Whelan"
Comment4 ""
$EndDescr
$Comp
L power:PWR_FLAG #FLG02
U 1 1 60088863
P 10550 1000
F 0 "#FLG02" H 10550 1075 50  0001 C CNN
F 1 "PWR_FLAG" H 10550 1173 50  0000 C CNN
F 2 "" H 10550 1000 50  0001 C CNN
F 3 "~" H 10550 1000 50  0001 C CNN
	1    10550 1000
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 600890D8
P 10150 1000
F 0 "#FLG01" H 10150 1075 50  0001 C CNN
F 1 "PWR_FLAG" H 10150 1173 50  0000 C CNN
F 2 "" H 10150 1000 50  0001 C CNN
F 3 "~" H 10150 1000 50  0001 C CNN
	1    10150 1000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR07
U 1 1 6008996A
P 10150 1000
F 0 "#PWR07" H 10150 850 50  0001 C CNN
F 1 "VCC" H 10165 1173 50  0000 C CNN
F 2 "" H 10150 1000 50  0001 C CNN
F 3 "" H 10150 1000 50  0001 C CNN
	1    10150 1000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR08
U 1 1 60089E8F
P 10550 1000
F 0 "#PWR08" H 10550 750 50  0001 C CNN
F 1 "GND" H 10555 827 50  0000 C CNN
F 2 "" H 10550 1000 50  0001 C CNN
F 3 "" H 10550 1000 50  0001 C CNN
	1    10550 1000
	1    0    0    -1  
$EndComp
$Sheet
S 3450 2250 1900 2250
U 60762533
F0 "CPU, Logic and bus" 50
F1 "cpu-logic-buffers.sch" 50
$EndSheet
$Sheet
S 1150 850  800  1000
U 60819514
F0 "Power and Reset" 50
F1 "power_reset.sch" 50
$EndSheet
$EndSCHEMATC
