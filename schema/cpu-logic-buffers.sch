EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx365:SN74HC365N IC?
U 1 1 6079F47D
P 8800 1350
AR Path="/6079F47D" Ref="IC?"  Part="1" 
AR Path="/60762533/6079F47D" Ref="IC1"  Part="1" 
F 0 "IC1" H 9350 1615 50  0000 C CNN
F 1 "SN74HC365N" H 9350 1524 50  0000 C CNN
F 2 "DIP794W53P254L1930H508Q16N" H 9750 1450 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc365" H 9750 1350 50  0001 L CNN
F 4 "Hex Buffers And Line Drivers With 3-State Outputs" H 9750 1250 50  0001 L CNN "Description"
F 5 "5.08" H 9750 1150 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 9750 1050 50  0001 L CNN "Manufacturer_Name"
F 7 "SN74HC365N" H 9750 950 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "595-SN74HC365N" H 9750 850 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/SN74HC365N?qs=RZS%252BpEsUn7rmmPzUbM6eGg%3D%3D" H 9750 750 50  0001 L CNN "Mouser Price/Stock"
F 10 "SN74HC365N" H 9750 650 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/sn74hc365n/texas-instruments" H 9750 550 50  0001 L CNN "Arrow Price/Stock"
	1    8800 1350
	1    0    0    -1  
$EndComp
Text GLabel 8600 1450 0    50   Input ~ 0
~RES
Wire Wire Line
	8600 1450 8800 1450
$Comp
L power:GND #PWR014
U 1 1 6079F485
P 8700 2300
F 0 "#PWR014" H 8700 2050 50  0001 C CNN
F 1 "GND" H 8705 2127 50  0000 C CNN
F 2 "" H 8700 2300 50  0001 C CNN
F 3 "" H 8700 2300 50  0001 C CNN
	1    8700 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 2050 8700 2050
Wire Wire Line
	8700 2050 8700 2300
Wire Wire Line
	8800 1350 8700 1350
Wire Wire Line
	8700 1350 8700 2050
Connection ~ 8700 2050
Text GLabel 8250 1550 0    50   Input ~ 0
~RES_BUS
Wire Wire Line
	8250 1550 8800 1550
Text GLabel 8600 1650 0    50   Input ~ 0
Phi2
Wire Wire Line
	8600 1650 8800 1650
Text GLabel 8250 1750 0    50   Input ~ 0
Phi2_BUS
Wire Wire Line
	8250 1750 8800 1750
$Comp
L power:VCC #PWR018
U 1 1 6079F496
P 10000 1100
F 0 "#PWR018" H 10000 950 50  0001 C CNN
F 1 "VCC" H 10015 1273 50  0000 C CNN
F 2 "" H 10000 1100 50  0001 C CNN
F 3 "" H 10000 1100 50  0001 C CNN
	1    10000 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 1100 10000 1350
Wire Wire Line
	10000 1350 9900 1350
$Comp
L power:GND #PWR019
U 1 1 6079F49E
P 10150 1300
F 0 "#PWR019" H 10150 1050 50  0001 C CNN
F 1 "GND" H 10155 1127 50  0000 C CNN
F 2 "" H 10150 1300 50  0001 C CNN
F 3 "" H 10150 1300 50  0001 C CNN
	1    10150 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	9900 1450 10150 1450
Wire Wire Line
	10150 1450 10150 1300
Text GLabel 10200 1550 2    50   Input ~ 0
~RW
Wire Wire Line
	10200 1550 9900 1550
Text GLabel 10400 1650 2    50   Input ~ 0
~RW_BUS
Wire Wire Line
	10400 1650 9900 1650
Text GLabel 10150 1750 2    50   Input ~ 0
~CSIO
Wire Wire Line
	10150 1750 9900 1750
Text GLabel 10400 1850 2    50   Input ~ 0
~CSIO_BUS
Wire Wire Line
	9900 1850 10400 1850
$Comp
L 65xx:W65C02SxP U1
U 1 1 6080E2FB
P 2550 2800
F 0 "U1" H 2200 1400 50  0000 C CNN
F 1 "W65C02SxP" V 2550 2800 50  0000 C CIB
F 2 "" H 2550 4800 50  0001 C CNN
F 3 "http://www.westerndesigncenter.com/wdc/documentation/w65c02s.pdf" H 2550 4700 50  0001 C CNN
	1    2550 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 6080E301
P 2550 4350
F 0 "#PWR06" H 2550 4100 50  0001 C CNN
F 1 "GND" H 2555 4177 50  0000 C CNN
F 2 "" H 2550 4350 50  0001 C CNN
F 3 "" H 2550 4350 50  0001 C CNN
	1    2550 4350
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 6080E307
P 2550 1250
F 0 "#PWR05" H 2550 1100 50  0001 C CNN
F 1 "VCC" H 2565 1423 50  0000 C CNN
F 2 "" H 2550 1250 50  0001 C CNN
F 3 "" H 2550 1250 50  0001 C CNN
	1    2550 1250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR04
U 1 1 6080E30D
P 1850 3050
F 0 "#PWR04" H 1850 2900 50  0001 C CNN
F 1 "VCC" H 1865 3223 50  0000 C CNN
F 2 "" H 1850 3050 50  0001 C CNN
F 3 "" H 1850 3050 50  0001 C CNN
	1    1850 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3050 1850 3100
Wire Wire Line
	1850 3100 1950 3100
Wire Wire Line
	1950 2500 1850 2500
$Comp
L power:VCC #PWR03
U 1 1 6080E316
P 1850 2350
F 0 "#PWR03" H 1850 2200 50  0001 C CNN
F 1 "VCC" H 1865 2523 50  0000 C CNN
F 2 "" H 1850 2350 50  0001 C CNN
F 3 "" H 1850 2350 50  0001 C CNN
	1    1850 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 2400 1850 2400
Wire Wire Line
	1850 2400 1850 2350
Wire Wire Line
	1850 2500 1850 2400
Connection ~ 1850 2400
$Comp
L Oscillator:ECS-2520MV-xxx-xx X1
U 1 1 6080E320
P 1300 1050
F 0 "X1" H 1744 1096 50  0000 L CNN
F 1 "ECS-2520MV-xxx-xx" H 1500 1350 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_ECS_2520MV-xxx-xx-4Pin_2.5x2.0mm" H 1750 700 50  0001 C CNN
F 3 "https://www.ecsxtal.com/store/pdf/ECS-2520MV.pdf" H 1125 1175 50  0001 C CNN
	1    1300 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 6080E326
P 1300 1350
F 0 "#PWR02" H 1300 1100 50  0001 C CNN
F 1 "GND" H 1305 1177 50  0000 C CNN
F 2 "" H 1300 1350 50  0001 C CNN
F 3 "" H 1300 1350 50  0001 C CNN
	1    1300 1350
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 6080E32C
P 1300 750
F 0 "#PWR01" H 1300 600 50  0001 C CNN
F 1 "VCC" H 1315 923 50  0000 C CNN
F 2 "" H 1300 750 50  0001 C CNN
F 3 "" H 1300 750 50  0001 C CNN
	1    1300 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1050 1700 1100
Wire Wire Line
	1700 1900 1950 1900
NoConn ~ 1950 2000
NoConn ~ 1950 3600
NoConn ~ 900  1050
$Comp
L 74xx:74HC00 U2
U 1 1 6080E337
P 5200 1100
F 0 "U2" H 5200 1425 50  0000 C CNN
F 1 "74HC00" H 5200 1334 50  0000 C CNN
F 2 "" H 5200 1100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 5200 1100 50  0001 C CNN
	1    5200 1100
	1    0    0    -1  
$EndComp
Text GLabel 1450 2100 0    50   Input ~ 0
Phi2
Wire Wire Line
	1450 2100 1950 2100
Text GLabel 4600 1000 0    50   Input ~ 0
Phi2
Wire Wire Line
	4900 1000 4600 1000
$Comp
L 74xx:74HC00 U2
U 2 1 6080E341
P 4550 1400
F 0 "U2" H 4550 1725 50  0000 C CNN
F 1 "74HC00" H 4550 1634 50  0000 C CNN
F 2 "" H 4550 1400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 4550 1400 50  0001 C CNN
	2    4550 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1400 4150 1300
Wire Wire Line
	4150 1300 4250 1300
Wire Wire Line
	4150 1400 4150 1500
Wire Wire Line
	4150 1500 4250 1500
Connection ~ 4150 1400
Wire Wire Line
	4900 1200 4850 1200
Wire Wire Line
	4850 1200 4850 1400
Text GLabel 6400 1100 2    50   Input ~ 0
~CSRAM
Wire Wire Line
	6400 1100 5500 1100
$Comp
L 74xx:74HC00 U2
U 3 1 6080E350
P 5250 2700
F 0 "U2" H 5250 3025 50  0000 C CNN
F 1 "74HC00" H 5250 2934 50  0000 C CNN
F 2 "" H 5250 2700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 5250 2700 50  0001 C CNN
	3    5250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 1400 4850 2050
Connection ~ 4850 1400
Wire Wire Line
	3600 1400 4150 1400
Text GLabel 6350 1750 2    50   Input ~ 0
A14RAM
Wire Wire Line
	3700 3000 3700 2800
Text GLabel 6400 1450 2    50   Input ~ 0
~OERAM
Wire Wire Line
	3700 1750 6100 1750
Wire Wire Line
	6400 1450 6100 1450
Wire Wire Line
	6100 1450 6100 1750
Connection ~ 6100 1750
Wire Wire Line
	6100 1750 6350 1750
Text GLabel 6400 2050 2    50   Input ~ 0
~OEROM
Text GLabel 6400 2200 2    50   Input ~ 0
~CEROM
Wire Wire Line
	6400 2050 6100 2050
Wire Wire Line
	6100 2050 6100 2200
Wire Wire Line
	6100 2200 6400 2200
Connection ~ 6100 2050
Wire Wire Line
	6100 2050 4850 2050
Wire Wire Line
	4850 2050 4850 2600
Wire Wire Line
	4850 2600 4950 2600
Connection ~ 4850 2050
Text GLabel 6450 2700 2    50   Input ~ 0
~CSIO
Wire Wire Line
	6450 2700 5550 2700
Wire Wire Line
	4950 2800 3700 2800
Connection ~ 3700 2800
Wire Wire Line
	3700 2800 3700 1750
Wire Notes Line
	6800 2550 6800 2850
Wire Notes Line
	6800 2850 6300 2850
Wire Notes Line
	6300 2850 6300 2550
Wire Notes Line
	6300 2550 6800 2550
Wire Notes Line
	6800 1950 6800 2350
Wire Notes Line
	6800 2350 6300 2350
Wire Notes Line
	6300 2350 6300 1950
Wire Notes Line
	6300 1950 6800 1950
Wire Notes Line
	6800 1850 6800 1000
Wire Notes Line
	6800 1000 6300 1000
Wire Notes Line
	6300 1000 6300 1850
Wire Notes Line
	6300 1850 6800 1850
Text Notes 6650 1000 0    50   ~ 0
RAM
Text Notes 6650 2450 0    50   ~ 0
ROM
Text Notes 6700 2950 0    50   ~ 0
IO
Text GLabel 1950 1400 0    50   Input ~ 0
~RES
Wire Wire Line
	1950 1400 1950 1600
$Comp
L 74xx:74HC245 U5
U 1 1 6080E381
P 9550 4950
F 0 "U5" H 9300 4250 50  0000 C CNN
F 1 "74HC245" V 9450 5150 50  0000 C CNN
F 2 "" H 9550 4950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 9550 4950 50  0001 C CNN
	1    9550 4950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC245 U4
U 1 1 6080E387
P 7450 4950
F 0 "U4" H 7200 4250 50  0000 C CNN
F 1 "74HC245" V 7350 5200 50  0000 C CNN
F 2 "" H 7450 4950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 7450 4950 50  0001 C CNN
	1    7450 4950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC245 U3
U 1 1 6080E38D
P 5100 5000
F 0 "U3" H 4850 4300 50  0000 C CNN
F 1 "74HC245" V 5000 5200 50  0000 C CNN
F 2 "" H 5100 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 5100 5000 50  0001 C CNN
	1    5100 5000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR09
U 1 1 6080E393
P 5100 4050
F 0 "#PWR09" H 5100 3900 50  0001 C CNN
F 1 "VCC" H 5115 4223 50  0000 C CNN
F 2 "" H 5100 4050 50  0001 C CNN
F 3 "" H 5100 4050 50  0001 C CNN
	1    5100 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4050 5100 4200
$Comp
L power:VCC #PWR012
U 1 1 6080E39A
P 7450 4050
F 0 "#PWR012" H 7450 3900 50  0001 C CNN
F 1 "VCC" H 7465 4223 50  0000 C CNN
F 2 "" H 7450 4050 50  0001 C CNN
F 3 "" H 7450 4050 50  0001 C CNN
	1    7450 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 4050 7450 4150
$Comp
L power:VCC #PWR016
U 1 1 6080E3A1
P 9550 4050
F 0 "#PWR016" H 9550 3900 50  0001 C CNN
F 1 "VCC" H 9565 4223 50  0000 C CNN
F 2 "" H 9550 4050 50  0001 C CNN
F 3 "" H 9550 4050 50  0001 C CNN
	1    9550 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 4050 9550 4150
$Comp
L power:GND #PWR017
U 1 1 6080E3A8
P 9550 6000
F 0 "#PWR017" H 9550 5750 50  0001 C CNN
F 1 "GND" H 9555 5827 50  0000 C CNN
F 2 "" H 9550 6000 50  0001 C CNN
F 3 "" H 9550 6000 50  0001 C CNN
	1    9550 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 5750 9550 5900
$Comp
L power:GND #PWR013
U 1 1 6080E3AF
P 7450 6050
F 0 "#PWR013" H 7450 5800 50  0001 C CNN
F 1 "GND" H 7455 5877 50  0000 C CNN
F 2 "" H 7450 6050 50  0001 C CNN
F 3 "" H 7450 6050 50  0001 C CNN
	1    7450 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 6080E3B5
P 5100 6050
F 0 "#PWR010" H 5100 5800 50  0001 C CNN
F 1 "GND" H 5105 5877 50  0000 C CNN
F 2 "" H 5100 6050 50  0001 C CNN
F 3 "" H 5100 6050 50  0001 C CNN
	1    5100 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 5800 5100 5850
Wire Wire Line
	7450 5750 7450 5850
Wire Wire Line
	4600 5500 4600 5850
Wire Wire Line
	4600 5850 5100 5850
Connection ~ 5100 5850
Wire Wire Line
	5100 5850 5100 6050
Wire Wire Line
	6850 5850 7450 5850
Connection ~ 7450 5850
Wire Wire Line
	7450 5850 7450 6050
Wire Wire Line
	6950 5450 6850 5450
Wire Wire Line
	6850 5450 6850 5850
Wire Wire Line
	8950 5900 9550 5900
Connection ~ 9550 5900
Wire Wire Line
	9550 5900 9550 6000
Wire Wire Line
	9050 5450 8950 5450
Wire Wire Line
	8950 5450 8950 5900
NoConn ~ 1950 3700
Text Label 3150 1800 0    50   ~ 0
_A2
Text Label 3150 1900 0    50   ~ 0
_A3
Text Label 3150 2000 0    50   ~ 0
_A4
Text Label 3150 2100 0    50   ~ 0
_A5
Text Label 3150 2200 0    50   ~ 0
_A6
Text Label 3150 2300 0    50   ~ 0
_A7
Text Label 3150 1700 0    50   ~ 0
_A1
Text Label 3150 1600 0    50   ~ 0
_A0
Entry Wire Line
	3400 1600 3500 1700
Wire Wire Line
	3400 1600 3150 1600
Wire Wire Line
	3150 1700 3400 1700
Wire Wire Line
	3150 1800 3400 1800
Wire Wire Line
	3150 1900 3400 1900
Wire Wire Line
	3150 2000 3400 2000
Wire Wire Line
	3150 2100 3400 2100
Wire Wire Line
	3150 2200 3400 2200
Wire Wire Line
	3150 2300 3400 2300
Entry Wire Line
	3400 2300 3500 2400
Entry Wire Line
	3400 2200 3500 2300
Entry Wire Line
	3400 2100 3500 2200
Entry Wire Line
	3400 2000 3500 2100
Entry Wire Line
	3400 1900 3500 2000
Entry Wire Line
	3400 1800 3500 1900
Entry Wire Line
	3400 1700 3500 1800
Text Label 7950 4450 0    50   ~ 0
_A0
Text Label 7950 4550 0    50   ~ 0
_A1
Text Label 7950 4650 0    50   ~ 0
_A2
Text Label 7950 4750 0    50   ~ 0
_A3
Text Label 7950 4850 0    50   ~ 0
_A4
Text Label 7950 4950 0    50   ~ 0
_A5
Text Label 7950 5050 0    50   ~ 0
_A6
Text Label 7950 5150 0    50   ~ 0
_A7
Entry Wire Line
	8100 5150 8200 5250
Wire Wire Line
	8100 5150 7950 5150
Wire Wire Line
	7950 5050 8100 5050
Wire Wire Line
	7950 4950 8100 4950
Wire Wire Line
	7950 4850 8100 4850
Wire Wire Line
	7950 4750 8100 4750
Wire Wire Line
	7950 4650 8100 4650
Wire Wire Line
	7950 4550 8100 4550
Wire Wire Line
	7950 4450 8100 4450
Entry Wire Line
	8100 4450 8200 4550
Entry Wire Line
	8100 4550 8200 4650
Entry Wire Line
	8100 4650 8200 4750
Entry Wire Line
	8100 4750 8200 4850
Entry Wire Line
	8100 4850 8200 4950
Entry Wire Line
	8100 4950 8200 5050
Entry Wire Line
	8100 5050 8200 5150
Text Label 10050 4450 0    50   ~ 0
_A8
Text Label 10050 4550 0    50   ~ 0
_A9
Text Label 10050 4650 0    50   ~ 0
_A10
Text Label 10050 4750 0    50   ~ 0
_A11
Text Label 10050 4850 0    50   ~ 0
_A12
Text Label 10050 5050 0    50   ~ 0
_A14
Text Label 10050 4950 0    50   ~ 0
_A13
Text Label 10050 5150 0    50   ~ 0
_A15
Wire Wire Line
	10050 4450 10300 4450
Wire Wire Line
	10050 4550 10300 4550
Wire Wire Line
	10050 4650 10300 4650
Wire Wire Line
	10050 4750 10300 4750
Wire Wire Line
	10050 4850 10300 4850
Wire Wire Line
	10050 4950 10300 4950
Wire Wire Line
	10050 5050 10300 5050
Wire Wire Line
	10050 5150 10300 5150
Entry Wire Line
	10300 5150 10400 5250
Entry Wire Line
	10300 4450 10400 4550
Entry Wire Line
	10300 4550 10400 4650
Entry Wire Line
	10300 4650 10400 4750
Entry Wire Line
	10300 4750 10400 4850
Entry Wire Line
	10300 4850 10400 4950
Entry Wire Line
	10300 4950 10400 5050
Entry Wire Line
	10300 5050 10400 5150
Text Label 3150 2400 0    50   ~ 0
_A8
Text Label 3150 2500 0    50   ~ 0
_A9
Text Label 3150 2600 0    50   ~ 0
_A10
Text Label 3150 2700 0    50   ~ 0
_A11
Text Label 3150 2800 0    50   ~ 0
_A12
Text Label 3150 2900 0    50   ~ 0
_A13
Text Label 3150 3000 0    50   ~ 0
_A14
Text Label 3150 3100 0    50   ~ 0
_A15
Wire Wire Line
	3150 2400 3400 2400
Wire Wire Line
	3150 2500 3400 2500
Wire Wire Line
	3150 2600 3400 2600
Wire Wire Line
	3150 2700 3400 2700
Wire Wire Line
	3150 2800 3400 2800
Wire Wire Line
	3150 2900 3400 2900
Entry Wire Line
	3400 3100 3500 3200
Entry Wire Line
	3400 3000 3500 3100
Entry Wire Line
	3400 2900 3500 3000
Entry Wire Line
	3400 2800 3500 2900
Entry Wire Line
	3400 2700 3500 2800
Entry Wire Line
	3400 2600 3500 2700
Entry Wire Line
	3400 2500 3500 2600
Entry Wire Line
	3400 2400 3500 2500
Wire Wire Line
	3150 3100 3400 3100
Wire Wire Line
	3400 3100 3400 3250
Wire Wire Line
	3400 3250 3600 3250
Wire Wire Line
	3600 1400 3600 3250
Text Label 3150 3300 0    50   ~ 0
_D0
Text Label 3150 3400 0    50   ~ 0
_D1
Text Label 3150 3500 0    50   ~ 0
_D2
Text Label 3150 3600 0    50   ~ 0
_D3
Text Label 3150 3700 0    50   ~ 0
_D4
Text Label 3150 3800 0    50   ~ 0
_D5
Text Label 3150 3900 0    50   ~ 0
_D6
Text Label 3150 4000 0    50   ~ 0
_D7
Wire Wire Line
	3150 3300 3350 3300
Wire Wire Line
	3150 3400 3350 3400
Wire Wire Line
	3150 3500 3350 3500
Wire Wire Line
	3150 3600 3350 3600
Wire Wire Line
	3150 3700 3350 3700
Wire Wire Line
	3150 3800 3350 3800
Wire Wire Line
	3150 3900 3350 3900
Wire Wire Line
	3150 4000 3350 4000
Entry Wire Line
	3350 3300 3450 3400
Text Label 5600 4500 0    50   ~ 0
_D0
Text Label 5600 4600 0    50   ~ 0
_D1
Text Label 5600 4700 0    50   ~ 0
_D2
Text Label 5600 4800 0    50   ~ 0
_D3
Text Label 5600 4900 0    50   ~ 0
_D4
Text Label 5600 5000 0    50   ~ 0
_D5
Text Label 5600 5100 0    50   ~ 0
_D6
Text Label 5600 5200 0    50   ~ 0
_D7
Wire Wire Line
	5600 5200 5800 5200
Wire Wire Line
	5600 5100 5800 5100
Wire Wire Line
	5600 5000 5800 5000
Wire Wire Line
	5600 4900 5800 4900
Wire Wire Line
	5600 4800 5800 4800
Wire Wire Line
	5600 4700 5800 4700
Wire Wire Line
	5600 4600 5800 4600
Wire Wire Line
	5600 4500 5800 4500
Entry Wire Line
	5800 5200 5900 5300
Entry Wire Line
	3350 3400 3450 3500
Entry Wire Line
	3350 3500 3450 3600
Entry Wire Line
	3350 3600 3450 3700
Entry Wire Line
	3350 3700 3450 3800
Entry Wire Line
	3350 3800 3450 3900
Entry Wire Line
	3350 3900 3450 4000
Entry Wire Line
	3350 4000 3450 4100
Entry Wire Line
	5800 4500 5900 4600
Entry Wire Line
	5800 4600 5900 4700
Entry Wire Line
	5800 4700 5900 4800
Entry Wire Line
	5800 4800 5900 4900
Entry Wire Line
	5800 4900 5900 5000
Entry Wire Line
	5800 5000 5900 5100
Entry Wire Line
	5800 5100 5900 5200
Text Label 4600 4500 2    50   ~ 0
D0
Text Label 4600 4600 2    50   ~ 0
D1
Text Label 4600 4700 2    50   ~ 0
D2
Text Label 4600 4800 2    50   ~ 0
D3
Text Label 4600 4900 2    50   ~ 0
D4
Text Label 4600 5000 2    50   ~ 0
D5
Text Label 4600 5100 2    50   ~ 0
D6
Text Label 4600 5200 2    50   ~ 0
D7
Wire Wire Line
	4600 5200 4450 5200
Wire Wire Line
	4600 5100 4450 5100
Wire Wire Line
	4600 5000 4450 5000
Wire Wire Line
	4600 4900 4450 4900
Wire Wire Line
	4600 4800 4450 4800
Wire Wire Line
	4600 4700 4450 4700
Wire Wire Line
	4600 4600 4450 4600
Wire Wire Line
	4600 4500 4450 4500
Entry Wire Line
	4350 5100 4450 5200
Entry Wire Line
	4350 5000 4450 5100
Entry Wire Line
	4350 4900 4450 5000
Entry Wire Line
	4350 4800 4450 4900
Entry Wire Line
	4350 4700 4450 4800
Entry Wire Line
	4350 4600 4450 4700
Entry Wire Line
	4350 4500 4450 4600
Entry Wire Line
	4350 4400 4450 4500
Text Label 4350 4350 2    50   ~ 0
D[0..7]
Wire Wire Line
	9050 4450 8800 4450
Wire Wire Line
	9050 4550 8800 4550
Wire Wire Line
	9050 4650 8800 4650
Wire Wire Line
	9050 4750 8800 4750
Wire Wire Line
	9050 4850 8800 4850
Wire Wire Line
	9050 4950 8800 4950
Wire Wire Line
	9050 5050 8800 5050
Wire Wire Line
	9050 5150 8800 5150
Entry Wire Line
	8700 5050 8800 5150
Text Label 9050 5150 2    50   ~ 0
A15
Text Label 9050 5050 2    50   ~ 0
A14
Text Label 9050 4950 2    50   ~ 0
A13
Text Label 9050 4850 2    50   ~ 0
A12
Text Label 9050 4750 2    50   ~ 0
A11
Text Label 9050 4650 2    50   ~ 0
A10
Text Label 9050 4550 2    50   ~ 0
A9
Text Label 9050 4450 2    50   ~ 0
A8
Text Label 6950 4450 2    50   ~ 0
A0
Text Label 6950 4550 2    50   ~ 0
A1
Text Label 6950 4650 2    50   ~ 0
A2
Text Label 6950 4750 2    50   ~ 0
A3
Text Label 6950 4850 2    50   ~ 0
A4
Text Label 6950 4950 2    50   ~ 0
A5
Text Label 6950 5050 2    50   ~ 0
A6
Text Label 6950 5150 2    50   ~ 0
A7
Wire Wire Line
	6950 5150 6650 5150
Wire Wire Line
	6950 5050 6650 5050
Wire Wire Line
	6950 4950 6650 4950
Wire Wire Line
	6950 4850 6650 4850
Wire Wire Line
	6950 4750 6650 4750
Wire Wire Line
	6950 4650 6650 4650
Wire Wire Line
	6950 4550 6650 4550
Wire Wire Line
	6950 4450 6650 4450
Entry Wire Line
	6550 5050 6650 5150
Entry Wire Line
	6550 4350 6650 4450
Entry Wire Line
	8700 4350 8800 4450
Entry Wire Line
	6550 4450 6650 4550
Entry Wire Line
	6550 4550 6650 4650
Entry Wire Line
	6550 4650 6650 4750
Entry Wire Line
	6550 4750 6650 4850
Entry Wire Line
	6550 4850 6650 4950
Entry Wire Line
	6550 4950 6650 5050
Entry Wire Line
	8700 4450 8800 4550
Entry Wire Line
	8700 4550 8800 4650
Entry Wire Line
	8700 4650 8800 4750
Entry Wire Line
	8700 4750 8800 4850
Entry Wire Line
	8700 4850 8800 4950
Entry Wire Line
	8700 4950 8800 5050
$Comp
L 74xx:74HC00 U2
U 4 1 6080E4A7
P 2800 5400
F 0 "U2" H 2800 5725 50  0000 C CNN
F 1 "74HC00" H 2800 5634 50  0000 C CNN
F 2 "" H 2800 5400 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 2800 5400 50  0001 C CNN
	4    2800 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 5400 3100 5400
Text GLabel 1400 2800 0    50   Input ~ 0
R~W
Wire Wire Line
	1950 2800 1600 2800
Wire Wire Line
	1600 2800 1600 5400
Wire Wire Line
	1600 5400 2400 5400
Wire Wire Line
	2400 5400 2400 5300
Wire Wire Line
	2400 5300 2500 5300
Connection ~ 1600 2800
Wire Wire Line
	1600 2800 1400 2800
Wire Wire Line
	2500 5500 2400 5500
Wire Wire Line
	2400 5500 2400 5400
Connection ~ 2400 5400
$Comp
L power:VCC #PWR015
U 1 1 6080E4B9
P 9050 5350
F 0 "#PWR015" H 9050 5200 50  0001 C CNN
F 1 "VCC" H 9065 5523 50  0000 C CNN
F 2 "" H 9050 5350 50  0001 C CNN
F 3 "" H 9050 5350 50  0001 C CNN
	1    9050 5350
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR011
U 1 1 6080E4BF
P 6950 5350
F 0 "#PWR011" H 6950 5200 50  0001 C CNN
F 1 "VCC" H 6965 5523 50  0000 C CNN
F 2 "" H 6950 5350 50  0001 C CNN
F 3 "" H 6950 5350 50  0001 C CNN
	1    6950 5350
	1    0    0    -1  
$EndComp
Wire Bus Line
	3450 4100 4350 4100
Wire Bus Line
	3500 3200 6550 3200
Connection ~ 6550 3200
Wire Bus Line
	6550 3200 8700 3200
Wire Bus Line
	5650 6350 8200 6350
Connection ~ 8200 6350
Wire Bus Line
	8200 6350 10400 6350
Wire Wire Line
	3150 3000 3700 3000
Wire Bus Line
	3450 3400 3450 4100
Wire Bus Line
	4350 4100 4350 5100
Wire Bus Line
	8700 3200 8700 5050
Wire Bus Line
	6550 3200 6550 5050
Wire Bus Line
	10400 4550 10400 6350
Wire Bus Line
	8200 4550 8200 6350
Wire Bus Line
	3500 1700 3500 3200
$EndSCHEMATC
