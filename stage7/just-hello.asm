;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; just-hello.asm

  org $8000

  jmp main

  include data.asm
  include io.asm

main:	

  ldx #$FF
  txs

  ;; Initialise ACIA
  lda #%00000000          ; 8-bit, 1 stop, 115200 baud
  sta ACIA1_CONTROL

  lda #%00000111          ; No parity, enable hw, no interrupts
  sta ACIA1_COMMAND

write:
  ldx #0
next_char:
wait_txd_empty:
  lda ACIA1_STATUS
  and #$10
  beq wait_txd_empty
  lda text,x
  beq read
  sta ACIA1_DATA
  inx
  jmp next_char
read:
  txa
  pha
  jsr delay_long
  pla
  tax
  jmp write

text:
  byte "Hello World!", $0d, $0a, $00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Includes

  include delay.asm
  include lcdfont.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Reset vector address
  org $fffc
  word main
  word $0000

