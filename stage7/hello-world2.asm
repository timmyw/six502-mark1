;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; hello.asm

  org $8000

  ;; Jump over any code added by the includes
  jmp main

  include utils.asm
  include io.asm

main:	

  init_stack

  jsr init_acia1

write:
  
  putstr text

read:
  jsr getch

  jmp write

text:
  byte "Hello World!", $0d, $0a, $00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Includes

  include data.asm
  include delay.asm
  include lcdfont.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Reset vector address
  org $fffc
  word main
  word $0000

