;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; hello.asm

  org $8000

  jmp main

  include data.asm
  include io.asm

main:	

  ldx #$FF
  txs

  jsr init_acia1

write:
  ldx text
;  jsr putstr

  ldx #0
next_char:
wait_txd_empty:
  lda text,x
  beq read
  jsr putch
;  jsr delay_6551
  inx
  jmp next_char
read:
  jsr getch
  jmp write


text:
  byte "Hello World!", $0d, $0a, $00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Includes

  include delay.asm
  include lcdfont.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Reset vector address
  org $fffc
  word main
  word $0000

