  ;; count.asm

;; Output port address
OPORT equ $4000

;; Delay counts
YCNT equ $ff
XCNT equ $ff

  org $8000

main:
  ;; Starting count of zero
  lda #$00

loop:
  tax
  lda hexfont,x									; Load segment font for ~x~
  sta OPORT											; Write it out
  txa														; Restore ~a~

	ldy #YCNT
delay
	ldx #XCNT
delay_1:
	dex
	bne delay_1
	dey
	bne delay

  clc 
  adc #1
  and #$f

  jmp loop

hexfont:

;; Bits controlling LED display segments.
SEG_A EQU (1<<0)
SEG_B EQU (1<<1)
SEG_C EQU (1<<2)
SEG_D EQU (1<<3)
SEG_E EQU (1<<4)
SEG_F EQU (1<<5)
SEG_G EQU (1<<6)

	FCB ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F) ; 0
	FCB ~(SEG_B|SEG_C)												 ; 1
	FCB ~(SEG_A|SEG_B|SEG_D|SEG_E|SEG_G)			 ; 2
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_G)			 ; 3
	FCB ~(SEG_B|SEG_C|SEG_F|SEG_G)						 ; 4
	FCB ~(SEG_A|SEG_C|SEG_D|SEG_F|SEG_G)			 ; 5
	FCB ~(SEG_A|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G) ; 6
	FCB ~(SEG_A|SEG_B|SEG_C)									 ; 7
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G) ; 8
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_F|SEG_G)						 ; 9
	FCB ~(SEG_A|SEG_B|SEG_C|SEG_E|SEG_F|SEG_G)			 ; A
	fcb ~(SEG_C|SEG_D|SEG_E|SEG_F|SEG_G)						 ; b
	FCB ~(SEG_D|SEG_E|SEG_G)												 ; C
	FCB ~(SEG_B|SEG_C|SEG_D|SEG_E|SEG_G)						 ; d
	FCB ~(SEG_A|SEG_D|SEG_E|SEG_F|SEG_G)						 ; E
	FCB ~(SEG_A|SEG_E|SEG_F|SEG_G)

  org $fffc
  word main
  word $0000
